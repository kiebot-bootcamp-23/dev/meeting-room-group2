package in.kiebot.mizoneBooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MizoneBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MizoneBookingApplication.class, args);
	}

}
