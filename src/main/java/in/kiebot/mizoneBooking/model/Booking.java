package in.kiebot.mizoneBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"date", "meeting_room_id","slot_id"})
})
public class Booking {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private Date date;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    @JsonIgnoreProperties(value = { "bookings" })
    private Employee employee;



    @ManyToOne
    @JoinColumn(name = "slot_id")
    @JsonIgnoreProperties(value = { "bookings" })
    private Slot slot;

    @ManyToOne
    @JoinColumn(name = "meeting_room_id")
    @JsonIgnoreProperties(value = { "bookings" })
    private MeetingRoom meetingRoom;


}
