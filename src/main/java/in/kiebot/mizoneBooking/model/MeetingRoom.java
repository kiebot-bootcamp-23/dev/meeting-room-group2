package in.kiebot.mizoneBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MeetingRoom {
    @Id
    @GeneratedValue
    private int id;
    private String name;


    @OneToMany(mappedBy = "meetingRoom",cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "meetingRoom" }, allowSetters = true)
    List<Booking> bookings;
}
