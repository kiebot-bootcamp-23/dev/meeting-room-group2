package in.kiebot.mizoneBooking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Slot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String startTime;

    private String endTime;


    @OneToMany(mappedBy = "slot",cascade = CascadeType.ALL)
    List<Booking> bookings;

    @JsonIgnore
    public List<Booking> getBookings() {
        return bookings;
    }
}
