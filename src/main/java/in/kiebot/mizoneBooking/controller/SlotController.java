package in.kiebot.mizoneBooking.controller;

import in.kiebot.mizoneBooking.model.Slot;
import in.kiebot.mizoneBooking.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1/slots")
public class SlotController {
    @Autowired
    SlotRepository slotRepository;

    @PostMapping()
    ResponseEntity creatSlot(@RequestBody Slot slot, UriComponentsBuilder uriComponentsBuilder) {
        final Slot newSlot = slotRepository.save(slot);
        final URI location = uriComponentsBuilder.path("/api/v1/").buildAndExpand(slot.getId()).toUri();
        return ResponseEntity.created(location).body(newSlot);
    };

    @GetMapping()
    List<Slot> getAllSlots() {
        List<Slot> allSlots = slotRepository.findAll();
        return allSlots;
    };

    @GetMapping(value = "/{id}")
    ResponseEntity getSingleSlot(@PathVariable("id") Long id) {
        Optional<Slot> slot = slotRepository.findById(id);
        if (slot.isPresent()){
            return ResponseEntity.ok(slot.get());
        }
        return ResponseEntity.notFound().build();
    };


    @PutMapping(value = "/{id}")
    ResponseEntity updateSlot(@PathVariable("id") Long id, @RequestBody Slot slot) {
        final Optional<Slot> existingSkill = slotRepository.findById(id);

        if (existingSkill.isEmpty()){
            return ResponseEntity.badRequest().body("Slot with this id not found");
        }

        if (slot.getName() == null){
            return ResponseEntity.badRequest().body("Slot must have a name");
        }

        if (slot.getStartTime() == null){
            return ResponseEntity.badRequest().body("Slot must have a start time");
        }

        if (slot.getEndTime() == null){
            return ResponseEntity.badRequest().body("Slot must have a end time");
        }

        Slot editSkill = existingSkill.get();
        editSkill.setName(slot.getName());
        editSkill.setId(slot.getId());
        editSkill.setEndTime(slot.getEndTime());
        editSkill.setStartTime(slot.getStartTime());
        return ResponseEntity.ok(
                slotRepository.save(editSkill));
    };

    @DeleteMapping(value = "/{id}")
    ResponseEntity deleteSlot(@PathVariable("id") Long id) {
        if (id == null){
            return ResponseEntity.badRequest().body("Slot not found with this id");
        }
        slotRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
