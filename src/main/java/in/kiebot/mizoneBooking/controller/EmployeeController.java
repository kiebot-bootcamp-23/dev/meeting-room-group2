package in.kiebot.mizoneBooking.controller;

import in.kiebot.mizoneBooking.model.Company;
import in.kiebot.mizoneBooking.model.Employee;
import in.kiebot.mizoneBooking.model.Slot;
import in.kiebot.mizoneBooking.repository.CompanyRepository;
import in.kiebot.mizoneBooking.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CompanyRepository companyRepository;


    @PostMapping("companies/{companyId}/employees")
    ResponseEntity<Employee> createEmployee(@RequestBody Employee employee, @PathVariable Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        employee.setCompany(company.get());
        final Employee newEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok().body(newEmployee);
    }

    @GetMapping("companies/{companyId}/employees")
    List<Employee> getAllEmployees() {
        List<Employee> allSlots = employeeRepository.findAll();
        return allSlots;
    }

    @GetMapping(value = "companies/employees/{id}")
    ResponseEntity getSingleEmployee(@PathVariable("id") Integer id) {
        Optional<Employee> employee = employeeRepository.findById(id);

        if (employee.isPresent()){
            return ResponseEntity.ok(employee.get());
        }
        return ResponseEntity.notFound().build();
    };

    @PutMapping(value = "companies/{companyId}/employees/{id}")
    ResponseEntity getSingleEmployee(@PathVariable Integer companyId, @PathVariable("id") Integer id, @RequestBody Employee employee) {
        Optional<Employee> existingEmployee = employeeRepository.findById(id);

        if(existingEmployee.isEmpty()) {
            return ResponseEntity.badRequest().body("Employee with this id not found");
        }

        Employee editedEmployee = existingEmployee.get();
        editedEmployee.setName(employee.getName());

        return ResponseEntity.ok(employeeRepository.save(editedEmployee));
    };

    @DeleteMapping(value = "companies/{companyId}/employees/{id}")
    ResponseEntity deleteEmployee(@PathVariable("id") Integer id, @PathVariable Integer companyId) {
        if (id == null){
            return ResponseEntity.badRequest().body("Employee not found with this id");
        }
        employeeRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
