package in.kiebot.mizoneBooking.controller;

import in.kiebot.mizoneBooking.model.Company;
import in.kiebot.mizoneBooking.model.MeetingRoom;
import in.kiebot.mizoneBooking.model.Slot;
import in.kiebot.mizoneBooking.repository.CompanyRepository;
import in.kiebot.mizoneBooking.repository.MeetingRoomRepository;
import in.kiebot.mizoneBooking.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/meeting-rooms")
public class MeetingRoomController {


    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @Autowired
    SlotRepository slotRepository;


    @GetMapping(value = "")
    List<MeetingRoom> getAllMeetingRoom() {
        return meetingRoomRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity getMeetingRoomById(@PathVariable("id") int id) {
        final Optional<MeetingRoom> OptionalMeetingRoom = meetingRoomRepository.findById(id);
        if (OptionalMeetingRoom.isPresent()) return ResponseEntity.ok(OptionalMeetingRoom);
        return ResponseEntity.badRequest().body("No such meeting room");
    }

    @PostMapping("")
    ResponseEntity createMeetingRoom(@RequestBody MeetingRoom meetingRoom) {
        if (meetingRoom.getName() != null) {
            return ResponseEntity.ok(meetingRoomRepository.save(meetingRoom));
        }
        return ResponseEntity.badRequest().body("Meeting room should have a name");
    }

    @PutMapping("/{id}")
    ResponseEntity updateMeetingRoom(@PathVariable("id") int id, @RequestBody MeetingRoom meetingRoom) {
        final Optional<MeetingRoom> optionalMeetingRoom = meetingRoomRepository.findById(id);
        MeetingRoom exisitingMeetingRoom = optionalMeetingRoom.get();
        if (exisitingMeetingRoom.getId() == id) return ResponseEntity.ok(meetingRoomRepository.save(meetingRoom));
        return ResponseEntity.badRequest().body("Meeting room does not exist");
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteMeetingRoom(@PathVariable("id") int id) {
        final Optional<MeetingRoom> optionalMeetingRoom = meetingRoomRepository.findById(id);
        if (optionalMeetingRoom.isPresent()) {
            meetingRoomRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.badRequest().body("Meeting room does not exist");
    }

    @GetMapping("/{id}/available-slots")
    ResponseEntity getAvailableSlots(@PathVariable("id") int id, @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date){
        List<Slot> allAvailableSlots = slotRepository.findAvailableSlot(date,id);
        return ResponseEntity.ok(allAvailableSlots);
    }
}
