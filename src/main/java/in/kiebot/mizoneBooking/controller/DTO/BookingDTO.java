package in.kiebot.mizoneBooking.controller.DTO;

import in.kiebot.mizoneBooking.model.Employee;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDTO {
    private int id;
    private String name;
    private Date date;
    private int employeeId;
    private int meetingRoomId;
    private long slotId;
}
