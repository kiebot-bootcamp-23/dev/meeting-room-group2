package in.kiebot.mizoneBooking.controller;

import in.kiebot.mizoneBooking.controller.DTO.BookingDTO;
import in.kiebot.mizoneBooking.model.*;
import in.kiebot.mizoneBooking.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/bookings")
public class BookingController {
    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    SlotRepository slotRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @GetMapping(value = "")
    List<Booking> getAllBookingRoom() {
        return bookingRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity getBookingById(@PathVariable("id") int id) {
        final Optional<Booking> optionalBookingRoom = bookingRepository.findById(id);
        if (optionalBookingRoom.isPresent()) return ResponseEntity.ok(optionalBookingRoom);
        return ResponseEntity.badRequest().body("No such booking");
    }



    @PostMapping("")
    ResponseEntity createBookingRoom(@RequestBody BookingDTO booking) {
//        System.out.println("ddd"+ booking.getSlot());

        Optional<Slot> existingSlot = slotRepository.findById(booking.getSlotId());
        Optional<Employee> existingEmployee = employeeRepository.findById(booking.getEmployeeId());
        Optional<MeetingRoom> existingMeetingRoom = meetingRoomRepository.findById(booking.getMeetingRoomId());
        if(existingSlot.isEmpty())
            return ResponseEntity.badRequest().body("No such slot");
        if(existingEmployee.isEmpty())
            return ResponseEntity.badRequest().body("No such employee");
        if(existingMeetingRoom.isEmpty())
            return ResponseEntity.badRequest().body("No such meeting room");
        Booking saveBooking = new Booking();

        saveBooking.setSlot(existingSlot.get());
        saveBooking.setEmployee(existingEmployee.get());
        saveBooking.setMeetingRoom(existingMeetingRoom.get());
        saveBooking.setDate(booking.getDate());
        saveBooking.setName(booking.getName());
        if (booking.getName() != null) {
            return ResponseEntity.ok(bookingRepository.save(saveBooking));
        }



        return ResponseEntity.badRequest().body("Booking should have a name");
    }

    @PutMapping("/{id}")
    ResponseEntity updateBookingRoom(@PathVariable("id") int id, @RequestBody Booking booking) {
        final Optional<Booking> optionalBooking = bookingRepository.findById(id);
        Booking existingBooking= optionalBooking.get();
        if (existingBooking.getId() == id) return ResponseEntity.ok(bookingRepository.save(booking));
        return ResponseEntity.badRequest().body("Booking does not exist");
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteBooking(@PathVariable("id") int id) {
        final Optional<Booking> optionalBookingRoom = bookingRepository.findById(id);
        if (optionalBookingRoom.isPresent()) {
            bookingRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.badRequest().body("booking does not exist");
    }
}
