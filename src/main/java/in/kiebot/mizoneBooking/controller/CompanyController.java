package in.kiebot.mizoneBooking.controller;

import in.kiebot.mizoneBooking.model.Company;
import in.kiebot.mizoneBooking.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/companies")
public class CompanyController {

    @Autowired
    CompanyRepository companyRepository;


    @GetMapping(value = "")
    List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity getCompanyById(@PathVariable("id") int id) {
        final Optional<Company> OptionalCompanies = companyRepository.findById(id);
        if (OptionalCompanies.isPresent()) return ResponseEntity.ok(OptionalCompanies);
        return ResponseEntity.badRequest().body("No such company");
    }

    @PostMapping("")
    ResponseEntity createCompany(@RequestBody Company company) {
        if (company.getName() != null) {
            return ResponseEntity.ok(companyRepository.save(company));
        }
        return ResponseEntity.badRequest().body("Company should have a name");
    }

    @PutMapping("/{id}")
    ResponseEntity updateCompany(@PathVariable("id") int id, @RequestBody Company company) {
        final Optional<Company> optionalCompany = companyRepository.findById(id);
        Company existingCompany = optionalCompany.get();
        if (existingCompany.getId() == id) return ResponseEntity.ok(companyRepository.save(company));
        return ResponseEntity.badRequest().body("Company does not exist");
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteCompany(@PathVariable("id") int id) {
        final Optional<Company> optionalCompanies = companyRepository.findById(id);
        if (optionalCompanies.isPresent()) {
            companyRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.badRequest().body("Company does not exist");
    }
}
