package in.kiebot.mizoneBooking.repository;

import in.kiebot.mizoneBooking.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Integer> {
}
