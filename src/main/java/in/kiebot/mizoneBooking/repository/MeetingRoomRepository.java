package in.kiebot.mizoneBooking.repository;

import in.kiebot.mizoneBooking.model.Booking;
import in.kiebot.mizoneBooking.model.MeetingRoom;
import in.kiebot.mizoneBooking.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface MeetingRoomRepository extends JpaRepository<MeetingRoom,Integer> {


}
