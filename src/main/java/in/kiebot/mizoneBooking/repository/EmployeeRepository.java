package in.kiebot.mizoneBooking.repository;

import in.kiebot.mizoneBooking.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
