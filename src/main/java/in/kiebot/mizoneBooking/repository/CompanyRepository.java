package in.kiebot.mizoneBooking.repository;

import in.kiebot.mizoneBooking.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company,Integer> {

}
