package in.kiebot.mizoneBooking.repository;

import in.kiebot.mizoneBooking.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface SlotRepository extends JpaRepository<Slot, Long> {
    @Query(value = "select s.id as id , s.end_time as end_time, s.name as name, s.start_time as start_time from slot s left join booking as b on  s.id= b.slot_id and date\\:\\:DATE = :date\\:\\:DATE and meeting_room_id = :roomId where b.slot_id is null" , nativeQuery = true)
    List<Slot> findAvailableSlot(Date date, int roomId);
}
